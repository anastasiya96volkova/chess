#include "game.h"
#include <QDebug>
#include <pieces.h>

Move::Move(int p1, int p2, int p3, int p4, int lf, int prom){
    this->p1 = p1;
    this->p2 = p2;
    this->p3 = p3;
    this->p4 = p4;
    this->lf = lf;
    this->prom = prom;
}

Game::Game(){
    this->board = new Board();
    this->reset();
}

Game::~Game(){
    //delete
}

Piece* Game::getPiece(int p){
    if(!this->board->getTile(p)->isPiece())
        return nullptr;
    return this->board->getTile(p)->getPiece();
}

Tile* Game::getTile(int p){
    return this->board->getTile(p);
}

//signals
void Game::start(bool w){
    if(this->running)
        return;
    this->reset();
    this->white = w;
    this->running = 0;
    emit net_start(static_cast<quint8>(0x03));
}

void Game::p1(int p, int prom){ //for our gui only so we only select pieces when its our turn, might add bool as ret typez
    if(!this->running || !((turn + white) % 2))
        return;
    if(!this->board->getTile(p)->isPiece() || this->board->getTile(p)->getPiece()->isWhite() != this->white) //no piece check -> seg fault :)
        return;
    for(int j = 0; j < 64; j++){
        board->getTile(j)->setHighl(0);
    }
    this->board->getTile(p)->getPiece()->setHighl(1); //may move that to check if its possible to move anywhere later, would have to abort tho
    std::vector<int> a = this->board->getTile(p)->getPiece()->canMove(this->board->getTile(), this->moves.size() > 0 ? this->moves.at(this->moves.size() - 1).p2 : -1, p);
    int b = a.size();
    int i = 0;
    while(i < b){
        if(this->isCheck(p, a.at(i)) == this->white){
            a.erase(a.begin() + i);
            b -= 1;
        }
        else
            ++i;
    } //copied in isMate
    if(b == 0)
        return;
    sel = p;
    for(i = 0; i < b; ++i){
        this->board->getTile(a.at(i))->setHighl(1);
    }
    int bb[64] = {};
    for(int i = 0; i < 64; i++){
        bb[i] = this->board->getTile(i)->isHighl() ? -2 : -1;
    }
    emit gui_update(bb);
    emit gui_on_p1(1);
}

void Game::p2(int p1, int p2, int prom){
    if(sel != p1)
        return;
    if(!this->running || !((this->turn + this->white) % 2) || p1 < 0)
        return;
    int m;
    if(this->board->getTile(p2)->isHighl())
        m = this->move(p1, p2, prom);
    for(int j = 0; j < 64; j++){
        this->board->getTile(j)->setHighl(0);
    }
    int t = this->isMate();
    /*if(t > -1)
        this->mate(t);
    if(this->isDead() || this->isStalemate() == ((turn + white + 1) % 2)) //need to test if stalemate
        this->draw();*/
    //add promotion
    int x[5] = {0, 4, 3, 2, 1};
    net_move(static_cast<quint8>(sel % 8), static_cast<quint8>((sel - (sel % 8)) / 8), static_cast<quint8>(p2 % 8),
                  static_cast<quint8>((p2 - (p2 % 8)) / 8), static_cast<quint8>((m % 16) * 2 + (t != this->white ? 1 : 0) + x[(m - (m % 16)) / 16 + 1] * 16));
 //   this->move_ok = 1;
    int bb[64] = {};
    for(int i = 0; i < 64; i++){
        if(this->board->getTile(i)->isPiece()){
            bb[i] = this->board->getTile(i)->getPiece()->getType() + this->board->getTile(i)->getPiece()->isWhite() * 6;
        }
        else
            bb[i] = -1;
    }
   emit gui_update(bb);
}


void Game::start_rec(quint8 a, quint8 b){
    if(a > 0x02 || this->running)
        return;
//    this->group = b;
    return;
    this->reset();
    this->white = a == 0x01 ? 0 : 1;
    this->running = 1;
    emit net_start_ok(b);
}

void Game::start_ok_rec(quint8 a){
    this->running = 1;
//    this->group = a;
}

void Game::move_rec(quint8 a, quint8 b, quint8 c, quint8 d, quint8 e){
    if(((turn + white) % 2)){
        emit net_move_ok(static_cast<quint8>(0x07));
        return;
    }
    int p1 = a + b * 8;
    int p2 = c + d * 8;
    if(p1 > 64 || !this->board->getTile(p1)->isPiece()){
        emit net_move_ok(static_cast<quint8>(0x03));
        return;
    }
    if(!this->board->getTile(p1)->getPiece()->canMove(this->board->getTile(), this->moves.size() > 0 ? this->moves.at(this->moves.size() - 1).p2 : -1, p1, p2)){
        emit net_move_ok(static_cast<quint8>(0x04));
        return;
    }
    if(this->board->getTile(p2)->isPiece() && 0x01 & e > 0){
        emit net_move_ok(static_cast<quint8>(0x06));
        return;
    }
    if(this->isCheck(p1, p2) == !this->white){
        emit net_move_ok(static_cast<quint8>(0x04));
        return;
    }
    int x[5] = {0, 3, 2, 1, 0};
    int z = this->move(p1, p2, x[(e & 0xF0) / 16]);
    int y = this->isMate();
    if(y == !this->white){
        emit net_move_ok(static_cast<quint8>(0x02));
        return;
    }
    if(z == 1){
        emit net_move_ok(static_cast<quint8>(0x01));
        return;
    }
    emit net_move_ok(static_cast<quint8>(0x00));
    return;
}

void Game::move_ok_rec(quint8 a){
 //   if(!this->move_ok)
        return;
  //  this->move_ok = 0;
    switch(a){
    case 0x00:
    case 0x01:
    case 0x02:
        //do something?? add blocker to wait for response
        break;
    case 0x03:
    case 0x04:
    case 0x05:
    case 0x06:
    case 0x07:
    case 0x08:
        //do something ui
        this->unmove();
        break;
    default:
        //do smth
        break;
    }
}


//private functions


void Game::reset(){
    this->board->reset();
    this->turn = 0;
    this->running = 0;
    this->sel = -1;
    this->moves.clear();
    this->Kingw = 4;
    this->Kingb = 60;
    this->lastFifty = 0;
 //   this->group = -1;
}

int Game::move(int p1, int p2, int prom){
    //check rules //we dont check rules here so that moving is more efficient, we assume the piece can be moved to p2
    if(!this->running) return 0;
    int p3 = -1;
    int p4 = -1;
    int lf = this->lastFifty;
    this->lastFifty++;
    if(this->board->getTile(p2)->isPiece()){
        this->board->getTile(p2)->getPiece()->move();
        board->addTaken(this->board->getTile(p2)->remPiece());
        p3 = p2;
        this->lastFifty = 0;
    }
    else
        this->board->getTile(p2)->remPiece();
    if(this->board->getTile(p1)->getPiece()->getType() == 0 && !this->board->getTile(p2)->isPiece() && (p1 - p2) % 8 != 0){
        p3 = p2 + (this->board->getTile(p1)->getPiece()->isWhite() ? 8 : -8);
        board->addTaken(this->board->getTile(p3)->remPiece());
    }
    this->board->getTile(p2)->setPiece(this->board->getTile(p1)->remPiece());
    this->board->getTile(p2)->getPiece()->move();
    if(this->board->getTile(p2)->getPiece()->getType() == 5 && p1 - p2 == 2){
        this->board->getTile(p1 - 1)->setPiece(this->board->getTile(p1 - 4)->remPiece());
        this->board->getTile(p1 - 1)->getPiece()->move();
        p3 = p1 - 4;
        p4 = p1 - 1;
    }
    if(this->board->getTile(p2)->getPiece()->getType() == 5 && p2 - p1 == 2){
        this->board->getTile(p1 + 1)->setPiece(this->board->getTile(p1 + 3)->remPiece());
        this->board->getTile(p1 + 1)->getPiece()->move();
        p3 = p1 + 3;
        p4 = p1 + 1;
    }
    if(this->board->getTile(p2)->getPiece()->getType() == 0 && (p2 - (p2 % 8)) / 8 == (this->board->getTile(p2)->getPiece()->isWhite() ? 7 : 0)){
        switch(prom){
        case 0:
            this->board->getTile(p2)->setPiece(std::make_unique<Queen>(this->board->getTile(p2)->remPiece().get()->isWhite()));
            break;
        case 1:
            this->board->getTile(p2)->setPiece(std::make_unique<Rook>(this->board->getTile(p2)->remPiece().get()->isWhite()));
            break;
        case 2:
            this->board->getTile(p2)->setPiece(std::make_unique<Knight>(this->board->getTile(p2)->remPiece().get()->isWhite()));
            break;
        case 3:
            this->board->getTile(p2)->setPiece(std::make_unique<Bishop>(this->board->getTile(p2)->remPiece().get()->isWhite()));
            break;
        default:
            this->board->getTile(p2)->setPiece(std::make_unique<Queen>(this->board->getTile(p2)->remPiece().get()->isWhite()));
            break;
        }

    }
    else
//        prom - 1;
    if(this->board->getTile(p2)->getPiece()->getType() == 5){
        if(this->board->getTile(p2)->getPiece()->isWhite())
            this->Kingw = p2;
        else
            this->Kingb = p2;
    }
    if(this->board->getTile(p2)->getPiece()->getType() == 0)
        this->lastFifty = 0;
    this->moves.push_back(Move(p1, p2, p3, p4, lf, prom));
    this->turn++;
    //qDebug() << "lf" << this->lastFifty << lf; //todo fix lastfifty
    return (p3 > -1 ? (p4 > -1 ? 2 : 1) : 0) + prom * 16; //0 not 1 tak 2 roch
    //todo threefold repetition: same pos 3 times
}

bool Game::unmove(){ //we modify the board a lot internally to check for check/mate/stalemate etc..
    if(!this->running) return 0;
    Move a = moves.at(moves.size() - 1);
    moves.pop_back();
    this->board->getTile(a.p1)->setPiece(this->board->getTile(a.p2)->remPiece());
    this->board->getTile(a.p1)->getPiece()->unmove();
    if(a.p3 != -1){
        if(a.p4 == -1)
            this->board->getTile(a.p3)->setPiece(this->board->remTaken());
        else
            this->board->getTile(a.p3)->setPiece(this->board->getTile(a.p4)->remPiece());
        this->board->getTile(a.p3)->getPiece()->unmove();
    }
    if(a.prom != -1)
        this->board->getTile(a.p1)->setPiece(std::make_unique<Pawn>(this->board->getTile(a.p1)->remPiece().get()->isWhite()));
    this->turn--;
    //qDebug() << "fl" << this->lastFifty << a.lf;
    this->lastFifty = a.lf;
    return 0;
}

Tile* Game::getKing(bool c){ //for gui
    if(c)
        return this->board->getTile(Kingw);
    return this->board->getTile(Kingb);
}

int Game::isCheck(){
    for(int i = 0; i < 64; i++){
        if(this->board->getTile(i)->isPiece()){
            if(this->board->getTile(i)->getPiece()->canMove(board->getTile(), this->moves.size() > 0 ? this->moves.at(this->moves.size() - 1).p2 : -1, i, Kingw))
                return 1;
            if(this->board->getTile(i)->getPiece()->canMove(board->getTile(), this->moves.size() > 0 ? this->moves.at(this->moves.size() - 1).p2 : -1, i, Kingb))
                return 0;
        }
    }
    return -1; //-1 no, 0 black, 1 white
}

int Game::isCheck(int p1, int p2){
    this->move(p1, p2, 0);
    int a = this->isCheck();
    this->unmove();
    return a; //-1 no, 0 black, 1 white
}

int Game::isMate(){
    int a = this->isCheck();
    if(a == -1)
        return -1;
    for(int i = 0; i < 64; i++){
        if(this->board->getTile(i)->isPiece() && this->board->getTile(i)->getPiece()->isWhite() == a){
            std::vector<int> b = this->board->getTile(i)->getPiece()->canMove(this->board->getTile(), this->moves.size() > 0 ? this->moves.at(this->moves.size() - 1).p2 : -1, i);
            int c = b.size();
            int k = 0;
            while(k < c){
                if(this->isCheck(i, b.at(k)) == this->white){
                    b.erase(b.begin() + k);
                    c -= 1;
                }
                else
                    ++k;
            } //copied from p1
            if(c > 0)
                return -1;
        }
    }
    //copied in isStalemate
    return a; //-1 no, 0 black, 1 white
}

int Game::isMate(int p1, int p2){
    this->move(p1, p2, 0);
    int a = this->isMate();
    this->unmove();
    return a; //-1 no, 0 black, 1 white
}

int Game::isStalemate(){
    int a = this->isCheck();
    if(a != -1)
        return -1;
    for(int i = 0; i < 64; i++){
        if(this->board->getTile(i)->isPiece() && this->board->getTile(i)->getPiece()->isWhite() == a){
            std::vector<int> b = this->board->getTile(i)->getPiece()->canMove(this->board->getTile(), this->moves.size() > 0 ? this->moves.at(this->moves.size() - 1).p2 : -1, i);
            int c = b.size();
            int k = 0;
            while(k < c){
                if(this->isCheck(k, b.at(k)) == this->white){
                    b.erase(b.begin() + k);
                    c -= 1;
                }
                else
                    ++k;
            } //copied from p1
            if(c > 0)
                return -1;
        }
    }
    //copied from isMate
    return a; //-1 no, 0 black, 1 white
}

int Game::isStalemate(int p1, int p2){
    this->move(p1, p2, 0);
    int a = this->isStalemate();
    this->unmove();
    return a;
}

int Game::isDead(){
    int a[6] = {0, 0, 0, 0, 0, 0}; //knight, bishop white, bishop black //might use std::array for easier comparison
    for(int i = 0; i < 64; i++){
        if(this->board->getTile(i)->isPiece()){
            switch(this->board->getTile(i)->getPiece()->getType() + this->board->getTile(i)->getPiece()->isWhite() * 6){
            case 1:
                a[i % 2]++;
                break;
            case 2:
                a[2]++;
                break;
            case 7:
                a[i % 2 + 3]++;
                break;
            case 8:
                a[5]++;
                break;
            case 5: case 11:
                break;
            default:
                return 0;
            }
        }
    }
    int b = 0;
    for(int i = 0; i < 6; i++)
        b += a[i];
    if(b == 0 || b == 1)
        return 1;
    if(b == 2 && a[0] == a[3] && a[1] == a[4] && a[2] == 0 && a[5] && 0)
        return 1;
    return 0;
}

int Game::isDead(int p1, int p2){
    this->move(p1, p2, 0);
    int a = this->isDead();
    this->unmove();
    return a;
}

int Game::isRepeat(){
    //check for thrice same board
    return this->lastFifty >= 50;
}

int Game::isRepeat(int p1, int p2){
    this->move(p1, p2, 0);
    int a = this->isRepeat();
    this->unmove();
    return a;
}

void Game::resign(bool white){
    if(!this->running) return;

    //do stuff when resign
    this->running = 0;
}

void Game::mate(bool white){
    if(!this->running) return;
    qDebug() << "mate" << white;
    //do stuff when mate
    this->running = 0;
}

void Game::draw(){
    if(!this->running) return;
    qDebug() << "draw";
    //do stuff when draw
    this->running = 0;
}
