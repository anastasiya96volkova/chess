#ifndef PIECE_H
#define PIECE_H
#pragma once
#include <vector>
#include "board.h"

class Tile;

class Piece { //piece type is int 0-pawn 1-bishop 2-knight 3-rook 4-queen 5-king
public:
    Piece(int t, bool c);
    bool isWhite() const;
    int getType() const;
    virtual std::vector<int> canMove(Tile** t, int l, int i1) = 0; //all tiles piece can move to
    virtual bool canMove(Tile** t, int l, int i1, int i2) = 0; //can move to tile
    bool setHighl(bool h);
    bool getHighl();
    int getMoves();
    void move(); //to increase moves count
    void unmove();
    virtual ~Piece() = default;
protected:
    int moves;
    int type; //only for gui
    bool white;
    bool highl;
    bool isIn(int x, int y);
    std::pair<int, int> to2(int i);
    int to1(int x, int y);
};

#endif // PIECE_H
