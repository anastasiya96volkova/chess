#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QIcon>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    for(int i = 0;i<8;++i)
    {
        for(int j=0;j<8;++j)
        {
            int id=i*8+j;
            field[id] = new QToolButton(this);
            field[id]->setMaximumHeight(99999);
            field[id]->setMaximumWidth(99999);

            if((id+(i%2))%2==1)
                field[id]->setStyleSheet("background: rgb(115,78,70)");
            else
                field[id]->setStyleSheet("background: rgb(213,194,165)");
            ui->gridLayout->addWidget(field[id],i,j);

            field[id]->show();
            connect(field[id],SIGNAL(clicked()),this,SLOT(onButtonClick()));
        }
    }
    SetFigures();
    buttonBox = new QDialogButtonBox(this);
    connect(buttonBox,SIGNAL(clicked()),this,SLOT(on_buttonBox_accepted()));
    connect(buttonBox,SIGNAL(clicked()),this,SLOT(on_buttonBox_rejected()));
    startChosen=false;
    endChosen=false;
}
void MainWindow::resizeEvent(QResizeEvent *event)
{
    int nh=event->size().height();
    int nw=event->size().width();
    width=nh*0.65;
    x_0=0.125*width;
    y_0=0.125*width;

    ui->checkboard->resize(width,width);
    ui->checkboard->move(x_0,y_0);
    for (int i=0;i<32;++i)
    {
        MoveFigure(i);
    }
    UpdatePictures();
    OK_w=nh*0.02;
    OK_h=nw*0.02;
    OK_x=0.582*nw;
    OK_y=0.775*nh;
 //   ui->selectionbox->resize(OK_w,OK_h); Button resize doesnt work
 //   ui->selectionbox->move(OK_x,OK_y);

}
void MainWindow::SetFigures()
{
    for(int i=0;i<16;++i)
    {
        fig[i].btn = new QToolButton(ui->checkboard);
        fig[i].id = i;
        fig[i].btn->show();
    }
    for(int i=0;i<16;++i)
    {
        fig[i+16].btn = new QToolButton(ui->checkboard);
        fig[i+16].id = 63-i;
        fig[i+16].btn->show();
    }
    for(int i=0;i<32;++i)
    {
        connect(fig[i].btn,SIGNAL(clicked()),this,SLOT(onButtonClick()));
    }
}
void MainWindow::MoveFigure(int fid)
{
    int x = fig[fid].id%8;
    int y = fig[fid].id/8;
    float x_new = (width/8.0f)*x;
    float y_new = (width/8.0f)*y;
    fig[fid].btn->move(static_cast<int>(x_new),static_cast<int>(y_new));
    fig[fid].btn->resize(width/8,width/8);
}
void MainWindow::UpdatePictures()
{
    int size=width/8;
        QSize sz(size,size);
        char name[256];

        for (int i=0;i<32;++i)
        {
        sprintf(name,"icons/%d.PNG",i);
        fig[i].picture.addFile(name);
        fig[i].btn->setIcon(fig[i].picture);
        fig[i].btn->setIconSize(sz);
        fig[i].btn->setStyleSheet("background: transparent");
       }
    startChosen=false;
    endChosen=false;
}
void MainWindow::onButtonClick()
{
    QToolButton *button = dynamic_cast<QToolButton*>(sender());
    if (button==nullptr) return;
    if (!(startChosen) && !(endChosen))
    {

        int index_s=0;
        for(;index_s<32;++index_s)
        {
            if(button==fig[index_s].btn) break;
        }
        if (index_s==32) return;
        move[0]=fig[index_s].id_old;
        sel_fid=index_s;
        startChosen=true;
        endChosen=false;
        fig[index_s].btn->setStyleSheet("background: rgba(0,0,180,40)");
        qDebug("start chosen");
        return;
    }
    else if (startChosen && !(endChosen))
    {
        int index_e=0;
        for(;index_e<64;++index_e)
        {
            if(button==field[index_e]) break;
        }
        if (index_e==64) return;
        move[1]=index_e;
        fig[sel_fid].id=move[1];
        MoveFigure(sel_fid);
        UpdatePictures();
        endChosen=true;
        qDebug("end chosen");
        return;
    }
    else
    {
        fig[sel_fid].id=move[0];
         fig[sel_fid].id_old=move[0];
        MoveFigure(sel_fid);
        UpdatePictures();
        startChosen=false;
        endChosen=false;
        return;
    }

}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_buttonBox_accepted()
{

    if((white && isWhiteTurn) || (!(white) && !(isWhiteTurn)))
    for(int i=0;i<16;++i)
    {
        emit log_p2(fig[i].id_old,fig[i].id,2);
    }
    else
    for (int j=0;j<16;++j)
    {
        emit log_p1(fig[j].id_old,2);
    }
}



void MainWindow::on_buttonBox_rejected()
{
        fig[sel_fid].id=move[0];
        fig[sel_fid].id_old=move[0];
        MoveFigure(sel_fid);
        UpdatePictures();
        qDebug("Move Cancelled");

}
void MainWindow::log_update(int bb[64])
{
    for(int i=0;i<64;++i)
    {

    }
}
void MainWindow::log_endgame()
{
    qDebug("end of game");
}
void MainWindow::log_on_p1(int n)
{
    qDebug("player1 mode");
}

void MainWindow::on_pushButton_clicked()
{
    emit resign(white);
    qDebug("you resigned");
}

