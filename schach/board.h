#ifndef BOARD_H
#define BOARD_H
#include <memory>
#pragma once
#include <array>
#include <map>
#include "piece.h"

class Piece;

class Tile
{
public:
    Tile(int i);
    bool isWhite() const;
    bool isHighl();
    bool setHighl(bool h);
    void setPiece(std::unique_ptr<Piece> p);
    Piece* getPiece();
    std::unique_ptr<Piece> remPiece();
    bool isPiece();
private:
    std::unique_ptr<Piece> piece; //instead of Piece*
    bool white;
    bool highl;
    int i; //debug
    ~Tile();
};

class Board
{
public:
    Board();
    Tile* getTile(int i);
    Tile** getTile(); //all tiles
    void addTaken(std::unique_ptr<Piece> p);
    std::unique_ptr<Piece> remTaken();
    void reset();
    ~Board();
private:
    std::vector<std::unique_ptr<Piece>> taken;
    std::array<Tile*,64> board;   //A1-0 A2-1 A3-2 .. B1-8 .. H8-63

};

#endif // BOARD_H
