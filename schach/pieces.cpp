#include "pieces.h"
#include "piece.h"
#include <QDebug>

Pawn::Pawn(bool c): Piece(0, c) {}

std::vector<int> Pawn::canMove(Tile** t, int l, int i1){
    std::vector<int> a;
    int x = to2(i1).first;
    int y = to2(i1).second;
    bool n = 1;
    for(int i = 1; i < (this->moves == 0 ? 3 : 2); i++){
        if(!isIn(x, y + i * (this->white ? 1 : -1)))
            continue;
        int z = to1(x, y + i * (this->white ? 1 : -1));
        if(n){
            if(!t[z]->isPiece()){
                a.push_back(z);
            }
            else
                n = 0;
        }
    }
    int m[2] = {1, -1};
    for(int i = 0; i < 2; i++){
        if(!isIn(x + (this->white ? 1 : -1), y + m[i]))
            continue;
        int z = to1(x + (this->white ? 1 : -1), y + m[i]);
        if(t[z]->isPiece() && t[z]->getPiece()->isWhite() != this->white)
            a.push_back(z);
    }
    for(int i = 0; i < 2; i++){
        if(!isIn(x + (this->white ? 2 : -2), y + m[i]))
            continue;
        int z = to1(x + (this->white ? 2 : -2), y + m[i]);
        if(z == l && t[z]->getPiece()->getMoves() == 1 && t[z]->getPiece()->getType() == 0 && t[z]->getPiece()->isWhite() != this->white)
            a.push_back(to1(x + (this->white ? 1 : -1), y + m[i]));
    }
    return a;
}

bool Pawn::canMove(Tile** t, int l, int i1, int i2){
    //todo placeholder for pot more efficient function
    std::vector<int> a = this->canMove(t, l, i1);
    for(size_t i = 0; i < a.size(); i++){
        if(i2 == a[i])
            return 1;
    }
    return 0; //cant move
}

Bishop::Bishop(bool c): Piece(1, c) {}

std::vector<int> Bishop::canMove(Tile** t, int l, int i1){
    l = -1; //en croissant
    std::vector<int> a;
    int x = to2(i1).first;
    int y = to2(i1).second;
    int m[8] = {1, 1, -1, -1, -1, 1, 1, -1};
    bool n[4] = {1, 1, 1, 1};
    for(int i = 1; i < 8; ++i){
        for(int j = 0; j < 4; ++j){
            int b = i * m[j * 2];
            int c = i * m[j * 2 + 1];
            if(!isIn(x + b, y + c))
                continue;
            int z = to1(x + b, y + c);
            if(n[j]){
                if(!t[z]->isPiece()){
                    a.push_back(z);
                }
                else
                    if(t[z]->getPiece()->isWhite() != this->white){
                        a.push_back(z);
                        n[j] = 0;
                    }
                    else
                        n[j] = 0;
            }}
    }
    return a;
}

bool Bishop::canMove(Tile** t, int l, int i1, int i2){
    //todo placeholder for pot more efficient function
    std::vector<int> a = this->canMove(t, l, i1);
    for(size_t i = 0; i < a.size(); i++){
        if(i2 == a[i])
            return 1;
    }
    return 0; //cant move
}

Knight::Knight(bool c): Piece(2, c) {}

std::vector<int> Knight::canMove(Tile** t, int l, int i1){
    l = -1; //en croissant
    std::vector<int> a;
    int x = to2(i1).first;
    int y = to2(i1).second;
    int m[16] = {2, 1, 2, -1, 1, 2, 1, -2, -1, 2, -1, -2, -2, 1, -2, -1};
    for(int i = 0; i < 8; ++i){
        int b = m[i * 2];
        int c = m[i * 2 + 1];
        if(!isIn(x + b, y + c))
            continue;
        int z = to1(x + b, y + c);
        if(!t[z]->isPiece()){
            a.push_back(z);
        }
        else
            if(t[z]->getPiece()->isWhite() != this->white){
                a.push_back(z);
            }
    }
    return a;
}

bool Knight::canMove(Tile** t, int l, int i1, int i2){
    //todo placeholder for pot more efficient function
    std::vector<int> a = this->canMove(t, l, i1);
    for(size_t i = 0; i < a.size(); i++){
        if(i2 == a[i])
            return 1;
    }
    return 0; //cant move
}

Rook::Rook(bool c): Piece(3, c) {}

std::vector<int> Rook::canMove(Tile** t, int l, int i1){
    l = -1; //en croissant
    std::vector<int> a;
    int x = to2(i1).first;
    int y = to2(i1).second;
    int m[8] = {1, 0, -1, 0, 0, 1, 0, -1};
    bool n[4] = {1, 1, 1, 1};
    for(int i = 1; i < 8; ++i){
        for(int j = 0; j < 4; ++j){
            int b = i * m[j * 2];
            int c = i * m[j * 2 + 1];
            if(!isIn(x + b, y + c))
                continue;
            int z = to1(x + b, y + c);
            if(n[j]){
                if(!t[z]->isPiece()){
                    a.push_back(z);
                }
                else
                    if(t[z]->getPiece()->isWhite() != this->white){
                        a.push_back(z);
                        n[j] = 0;
                    }
                    else
                        n[j] = 0;
            }
        }
    }
    return a;
}

bool Rook::canMove(Tile** t, int l, int i1, int i2){
    //todo placeholder for pot more efficient function
    std::vector<int> a = this->canMove(t, l, i1);
    for(size_t i = 0; i < a.size(); i++){
        if(i2 == a[i])
            return 1;
    }
    return 0; //cant move
}

Queen::Queen(bool c): Piece(4, c) {}

std::vector<int> Queen::canMove(Tile** t, int l, int i1){
    l = -1; //en croissant
    std::vector<int> a;
    int x = to2(i1).first;
    int y = to2(i1).second;
    int m[16] = {1, 0, -1, 0, 0, 1, 0, -1, 1, 1, -1, -1, -1, 1, 1, -1};
    bool n[8] = {1, 1, 1, 1, 1, 1, 1, 1};
    for(int i = 1; i < 8; ++i){
        for(int j = 0; j < 8; ++j){
            int b = i * m[j * 2];
            int c = i * m[j * 2 + 1];
            if(!isIn(x + b, y + c))
                continue;
            int z = to1(x + b, y + c);
            if(n[j]){
                if(!t[z]->isPiece()){
                    a.push_back(z);
                }
                else
                    if(t[z]->getPiece()->isWhite() != this->white){
                        a.push_back(z);
                        n[j] = 0;
                    }
                    else
                        n[j] = 0;
            }}
    }
    return a;
}

bool Queen::canMove(Tile** t, int l, int i1, int i2){
    //todo placeholder for pot more efficient function
    std::vector<int> a = this->canMove(t, l, i1);
    for(size_t i = 0; i < a.size(); i++){
        if(i2 == a[i])
            return 1;
    }
    return 0; //cant move
}

King::King(bool c): Piece(5, c) {}

std::vector<int> King::canMove(Tile** t, int l, int i1){
    l = -1; //en croissant
    std::vector<int> a;
    int x = to2(i1).first;
    int y = to2(i1).second;
    for(int i = -1; i < 2; ++i){
        for(int j = -1; j < 2; ++j){
            if(i == 0 && j == 0)
                continue;
            if(!isIn(x + i, y + j))
                continue;
            int z = to1(x + i, y + j);
            if(!t[z]->isPiece()){
                a.push_back(z);
            }
            if(t[z]->isPiece() && t[z]->getPiece()->isWhite() != this->white){
                a.push_back(z);
            }
        }
    }
    if(this->getMoves() == 0 && !t[i1 - 1]->isPiece() && !t[i1 - 2]->isPiece() && !t[i1 - 3]->isPiece() &&
            t[i1 - 4]->isPiece() && t[i1 - 4]->getPiece()->getMoves() == 0) //manual if but works so nyeh
        a.push_back(i1 - 2);
    if(this->getMoves() == 0 && !t[i1 + 1]->isPiece() && !t[i1 + 2]->isPiece() &&
            t[i1 + 3]->isPiece() && t[i1 + 3]->getPiece()->getMoves() == 0)
        a.push_back(i1 + 2);
    return a;
}

bool King::canMove(Tile** t, int l, int i1, int i2){
    //todo placeholder for pot more efficient function
    std::vector<int> a = this->canMove(t, l, i1);
    for(size_t i = 0; i < a.size(); i++){
        if(i2 == a[i])
            return 1;
    }
    return 0; //cant move
}
