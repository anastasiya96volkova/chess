#ifndef PIECES_H
#define PIECES_H
#pragma once
#include <piece.h>

class Pawn: public Piece {
public:
    Pawn(bool c);
    std::vector<int> canMove(Tile** t, int l, int i1) override;
    bool canMove(Tile** t, int l, int i1, int i2) override;
    ~Pawn() override = default;
private:
};

class Bishop: public Piece {
public:
    Bishop(bool c);
    std::vector<int> canMove(Tile** t, int l, int i1) override;
    bool canMove(Tile** t, int l, int i1, int i2) override;
    ~Bishop() override = default;
};

class Knight: public Piece {
public:
    Knight(bool c);
    std::vector<int> canMove(Tile** t, int l, int i1) override;
    bool canMove(Tile** t, int l, int i1, int i2) override;
    ~Knight() override = default;
};

class Rook: public Piece {
public:
    Rook(bool c);
    std::vector<int> canMove(Tile** t, int l, int i1) override;
    bool canMove(Tile** t, int l, int i1, int i2) override;
    ~Rook() override = default;
};

class Queen: public Piece {
public:
    Queen(bool c);
    std::vector<int> canMove(Tile** t, int l, int i1) override;
    bool canMove(Tile** t, int l, int i1, int i2) override;
    ~Queen() override = default;
};

class King: public Piece {
public:
    King(bool c);
    std::vector<int> canMove(Tile** t, int l, int i1)  override;
    bool canMove(Tile** t, int l, int i1, int i2) override;
    ~King() override = default;
};

#endif // PIECES_H
