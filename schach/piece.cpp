#include "piece.h"
#include <QDebug>

Piece::Piece(int t, bool c){
    this->moves = 0;
    this->highl = 0;
    this->type = t;
    this->white = c;
}

bool Piece::isWhite() const{
    return this->white;
}

int Piece::getType() const{
    return this->type;
}

bool Piece::setHighl(bool h){
    this->highl = h;
    return this->highl;
}

bool Piece::getHighl(){
    return this->highl;
}

int Piece::getMoves(){
    return this->moves;
}

void Piece::move(){
    this->moves++;
}

void Piece::unmove(){
    this->moves--;
}

bool Piece::isIn(int x, int y){
    return x > -1 && x < 8 && y > -1 && y < 8;
}

std::pair<int, int> Piece::to2(int i){
    int x = i % 8;
    return std::pair<int, int>(x, (i - x) / 8);
}

int Piece::to1(int x, int y){
    return y * 8 + x;
}
