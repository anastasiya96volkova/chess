#ifndef CONFIG_MENU_H
#define CONFIG_MENU_H

#include <QMainWindow>
#include <QLineEdit>
#include "ui_config_menu.h"
#include "mainwindow.h"
namespace Ui {
class config_menu;
}

class config_menu : public QMainWindow
{
    Q_OBJECT
    bool server;
    quint16 port;
    QString address,port_str;
    QLineEdit lineEdit1, lineEdit2;
    MainWindow window;
    bool white;

public:
    explicit config_menu(QWidget *parent = nullptr);

    ~config_menu();

private slots:
    void on_radioButton_clicked();
    void on_radioButton_2_clicked();
    void on_pushButton_clicked();
    void on_checkBox2_clicked();
    void on_checkBox_clicked();
public slots:

    void net_client_con();
    void net_port_inv();
    void net_server_con();

public:
signals:
    void net_connect();
    void net_disconnect();
    void net_setPort(quint16 port);
    void net_setIp(QString ip);
    void net_isServer(bool server);


private:
    Ui::config_menu *ui;
};

#endif // CONFIG_MENU_H
