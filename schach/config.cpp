#include "config.h"
#include <QList>

config_menu::config_menu(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::config_menu)
{
    ui->setupUi(this);
}

config_menu::~config_menu()
{
    delete ui;
}
void config_menu::on_radioButton_clicked()
{
    server=true;

}
void config_menu::on_radioButton_2_clicked()
{
    server = false;
}
void config_menu::on_checkBox2_clicked()
{
    white=true;
}
void config_menu::on_checkBox_clicked()
{
    white=false;
}
void config_menu::on_pushButton_clicked()
{
    emit window.log_start(white);
    emit net_isServer(server);
    emit lineEdit2.textEdited(port_str);
    port=static_cast<quint16>(port_str.toUInt());
    emit net_setPort(port);
    emit lineEdit1.textEdited(address);
    emit net_setIp(address);
    emit net_connect();
}
void config_menu::net_server_con()
{
    qDebug("server connected");
}
void config_menu::net_port_inv()
{
    qDebug("port is invalid");
}
void config_menu::net_client_con()
{
    qDebug("client connected");
}

