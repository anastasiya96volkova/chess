﻿#include "schach.h"

Chess::Chess(QWidget *parent) : QWidget(parent),
                                _socket(new QTcpSocket(this))

{

    _stream.setByteOrder(QDataStream::BigEndian);

    //connect(_socket, SIGNAL(connected()), this, SLOT(succesfullyConnected()));
    //connect(_socket, SIGNAL(disconnected()), this, SLOT(succesfullyDisconnected()));
}



Chess::~Chess(){
  //  delete server;
    //delete _socket;
}



//Ui calls this slot and tell me if the Player choosed the player or client mode
void Chess::setServerClient(bool _isServer){

    this->_isServer = _isServer;
}



//ui calls this slots to give me the port that the player wrote
void Chess::getPort(quint16 port){

    this->port = port;
}



//ui calls this slot to give me the address in case that the Player chooses Client
void Chess::getAddress(QString address){

    if(!_isServer){

        this->address = address;
    }
}




//ui calls this slot when the Player pushes the Connect Button on the main window
void Chess::connectServerClient(){


    if(_isServer)
    {
        if(port<1024){

            //prints out an error message, Ana needs to implement the slot on the
            //user Interface

            emit invalidPort();
            return;
        }

        else{

            if (!_connected){

            server = new QTcpServer(this);

            connect(server,SIGNAL(newConnection()), this, SLOT(newConnection()));

                if(!server->listen(QHostAddress::Any, port))
                {
                    qDebug()<<"Server could not start";
                }
                else{
                    qDebug()<<"Server started";
                }

            }

        }
    }




    else if (!_isServer){


        if(port<1024){

            //prints out an error message, Ana needs to implement the slot on the
            //user Interface

            emit invalidPort();
        return;
        }

        else {

            if (!_connected){

                _socket->connectToHost(address, port);
                connect(_socket, SIGNAL(connected()), this, SLOT(enableConnection()));
                connect(_socket, SIGNAL(disconnected()),this, SLOT(disconnectServerClient()));

        }
    }
    }

    else { return; }

}



void Chess::newConnection(){
    _socket = server->nextPendingConnection();

    //Player succesfully connected as the server, send signal to Ui to print
    //out a message about the connection and to call a slot to start the game

    assert(QObject::sender() != nullptr);

    emit serverConnected();

    _stream.setDevice(dynamic_cast<QTcpSocket*>(QObject::sender()));

    //connect(QObject::sender(), SIGNAL(QIODevice::readyRead()), this, SLOT(receive()));
    connect(_socket, SIGNAL(readyRead()), this, SLOT(receive()));

    _connected = true;

}



//Ui calls this slot if the player decides to disconnect from the game
void Chess::disconnectServerClient(){

    if(_connected && !_isServer){

        _socket->disconnectFromHost();

        //send signal to Ui to print out a message about the disconnection
        //from Server
        emit clientSuccesfullyDisconnected();
    }

    else if (_connected && _isServer){
        server->close();

        //send signal to Ui to print out a message about the disconnection
        //of the server
        emit serverSuccesfullyDisconnected();

    }
}



void Chess::enableConnection(){

    assert(QObject::sender() != nullptr);

    //_stream.setDevice(dynamic_cast<QTcpSocket*>(QObject::sender()));


    if(!_isServer){

        _connected = true;

        //Player succesfully connected as the client, send signal to Ui to print
        //out a message about the connection
        emit clientConnected();

        _stream.setDevice(_socket);

        connect(_socket, SIGNAL(readyRead()), this, SLOT(receive()));

        //connect(_socket, SIGNAL(readyRead()), this, SLOT(receive()));

    }
    else { return ;}
}


void Chess::receive(){

    _stream.setByteOrder(QDataStream::BigEndian);

    quint8 cmd;
    quint8 lenght;

    _stream>>cmd;
    _stream>>lenght;


    if(cmd==1 && lenght==2 && !_isServer){

            if(_stream.device()->bytesAvailable()==2){

            quint8 beginner, groupNumber;
            _stream>>beginner>>groupNumber;

                if ((beginner==0 || beginner==1) && groupNumber>0 && groupNumber<=4){

                    // send the received data to logic for the cmd 1
                    emit spielbeginn(beginner, groupNumber);

                }
                else {return;}
            }
        }


    else if(cmd==2 && lenght==1 &&_isServer){

        if(_stream.device()->bytesAvailable()==1){

             quint8 groupNumber;
             _stream>>groupNumber;

            if(groupNumber>0 && groupNumber<=4){

                // send the received data to logic for the cmd 2
                emit answerSpielbeginn(groupNumber);

            }
            else {return ;}
        }   
    }


    else if(cmd ==3 && lenght==5){

        if(_stream.device()->bytesAvailable()==5){

            quint8 column, row, endColumn, endRow, addInfo;

            _stream >>column>>row>>endColumn>>endRow>>addInfo;

            if(((column ==0 || column>0) && column<8 ) && ((row ==0 || row>0) && row<8) &&
                ((endColumn ==0 || endColumn>0) && endColumn<8) && ((endRow ==0 || endRow>0) &&
                 endRow<8)  && ((addInfo ==0 || addInfo>0) && addInfo<=44)){

                // send the received data to logic for the cmd 3
                emit receiveMove(column, row, endColumn, endRow, addInfo);
            }

            else {return; }
        }
    }





    else if (cmd==4 && lenght==1){

        if(_stream.device()->bytesAvailable()==1){

            quint8 status;

            _stream >>status;

            if( (status ==0 || status>0) && status<9 && status!=5){              

                // send the received data to logic for the cmd 4
                emit responseMove(status);

            }

            else {return;}
        }

   }

    else {return;}
}



//Logic sends a signal to start the game. The signal is gonna call my SLOT gameStart
void Chess::gameStart(quint8 beginner){

    //if(cmd==1 && _isServer){

    quint8 cmd =1, lenght=2, groupNumber=3;

    _stream <<cmd<<lenght<<beginner<<groupNumber;
    //}

}


//Logic sends a signal to answer to start the game. The signal is gonna call my SLOT answerStart
void Chess::answerStart(){

    //if(cmd==2 && !_isServer){

    quint8 cmd = 2, lenght = 1, groupNumber = 3;

    _stream <<cmd<<lenght<<groupNumber;
    //}



}


//Logic sends Netzwerk a signal to send a move. The signal is gonna call my SLOT sendMove
void Chess::sendMove(quint8 column, quint8 row,

quint8 endColumn, quint8 endRow, quint8 addInfo){

    //if(cmd==3){

    //_stream <<cmd<<lenght<<column<<row<<endColumn<<endRow<<addInfo;
    _stream <<quint8(3)<<quint8(5)<<column<<row<<endColumn<<endRow<<addInfo;
//}

}


//Logic sends Netzwerk a signal to send a move. The signal is gonna call my SLOT sendMove
void Chess::sendAnswer(quint8 status){

    //if(cmd==4){

    _stream <<quint8(4)<<quint8(1)<<status;

   // }

}
