#include "board.h"
#include "pieces.h"
#include <algorithm>
#include <QDebug>

Board::Board(){
    for(int i = 0; i < 64; ++i){
        this->board[i] = new Tile(i);
    }
}

Board::~Board(){
    //delete
}

Tile* Board::getTile(int i){
    return this->board[i];
}

Tile** Board::getTile(){
    return &this->board[0];
}


void Board::addTaken(std::unique_ptr<Piece> p){
    taken.push_back(std::move(p));
}

std::unique_ptr<Piece> Board::remTaken(){
    std::unique_ptr<Piece> a = std::move(taken.at(taken.size() - 1));
    taken.pop_back();
    return a;
}

void Board::reset(){
    for(int i = 0; i < 64; ++i){
        this->board[i]->remPiece().reset();
    }
    for(size_t i = 0; i < taken.size(); ++i)
        this->taken[i].reset();
    this->taken.clear();
    this->board[0]->setPiece(std::make_unique<Rook>(1));
    this->board[1]->setPiece(std::make_unique<Knight>(1));
    this->board[2]->setPiece(std::make_unique<Bishop>(1));
    this->board[3]->setPiece(std::make_unique<Queen>(1));
    this->board[4]->setPiece(std::make_unique<King>(1));
    this->board[5]->setPiece(std::make_unique<Bishop>(1));
    this->board[6]->setPiece(std::make_unique<Knight>(1));
    this->board[7]->setPiece(std::make_unique<Rook>(1));
    this->board[56]->setPiece(std::make_unique<Rook>(0));
    this->board[57]->setPiece(std::make_unique<Knight>(0));
    this->board[58]->setPiece(std::make_unique<Bishop>(0));
    this->board[59]->setPiece(std::make_unique<Queen>(0));
    this->board[60]->setPiece(std::make_unique<King>(0));
    this->board[61]->setPiece(std::make_unique<Bishop>(0));
    this->board[62]->setPiece(std::make_unique<Knight>(0));
    this->board[63]->setPiece(std::make_unique<Rook>(0));
    for(int i = 0; i < 8; i++){
        this->board[i + 8]->setPiece(std::make_unique<Pawn>(1));
        this->board[i + 48]->setPiece(std::make_unique<Pawn>(0));
    }
}


Tile::Tile(int i){
    this->white = i % 2;
    this->i = i;
}

bool Tile::isWhite() const{
    return this->white;
}

bool Tile::isHighl(){
    return this->highl;
}

bool Tile::setHighl(bool h){
    this->highl = h;
    return this->highl;
}

void Tile::setPiece(std::unique_ptr<Piece> p){
    this->piece = std::move(p); //we do not check if a piece already exists here
    return;
}

Piece* Tile::getPiece(){
    return this->piece.get();
}

std::unique_ptr<Piece> Tile::remPiece(){
    std::unique_ptr<Piece> rem = std::move(this->piece);
    return rem;
}

bool Tile::isPiece(){
    if(this->piece.get() == nullptr)
        return 0;
    return 1;
    //used for pointers, prob not needed for unique pointers
}

