#include <iostream>
#include <QNetworkInterface>
#include "chess.h"
#include <QWidget>
#include <QtNetwork/QTcpSocket>
#include <QDataStream>


Chess::Chess(QWidget *parent) : QWidget(parent), _socket(new QTcpSocket(this), _ui(new Ui::Chess){}

_ui->setupUi(this);

//_ui->pb is a button on the User Interface and it allows the player to choose
//between Server or Client mode
//pbServer and pbClient buttons needed
connect(_ui->pbServer, SIGNAL(clicked()), this, SLOT(setServer()));
connect(_ui->pbClient, SIGNAL(clicked()), this, SLOT(setClient()));

//pbConnect and pbDisconnect buttons needed
connect(_ui->pbConnect, SIGNAL(clicked()), this, SLOT(connectToServer()));
connect(_ui->pbDisconnect, SIGNAL(clicked()),this, SLOT(disconnectFromServer()));


_stream.setByteOrder(QDataStream::BigEndian);
auto _socket = new QTcpSocket(this);
QDataStream _stream;

Chess::~Chess(){
delete _ui;
_ui = nullptr;
}


bool _isServer;
bool _isPlayer;



void setServer(){
_isServer = TRUE;
}


void setClient(){
_isPlayer = TRUE;
}


void connectToServer(){
if(_isServer && !_isPlayer){
//prints out the IP Adress on the User Interface
//"leAdress" widget is needed
_ui->leAdress->appendPlainText("IP_Adress: 127.0.0.1");

//reads the port given by the player on the User Interface
//"lePort" button is needed
quint16 port = _ui->lePort->text()->toShort();

if(port<1024){
//prints out an error message
//"lePrint" could be any notificacion's message printes on the player's screen
_ui->lePrint->appendPlainText("Invalid Port Number.");
return;
}

auto socket = new QTcpSocket(this);
//"lePrint" could be any notificacion's message printes on the player's screen
    _ui->lePrint->appendPlainText("Connecting to IP_Adress: 127.0.0.1:" +
                                  _ui->lePort->text());

    qDebug() << "Connecting to port " << port << "\n";

_socket->connectToHost("127.0.0.1", port);
}


else (!_isServer && _isPlayer){
//reads the port given by the player on the User Interface
//"lePort" button is needed
quint16 port = _ui->lePort->text()->toShort();

if(port<1024){
//pints out an error message
//"lePrint" could be any notificacion's message printes on the player's screen
_ui->leprint->appendPlainText("Invalid Port Number.");
return;
}

_ui->teLog->appendPlainText("Conecting to "+ _ui->leHost->text() + ":" + _ui->lePort->text());
_socket->connectToHost("127.0.0.1", port);
}

connect(_socket, SIGNAL(connected()), this, SLOT(enableConnection()));
    connect(_socket, SIGNAL(disconnected()),this, SLOT(disconnected()));

}

void disconnectFromServer(){
_socket->disconnectFromHost();
}




void enableConnection(){
assert(QObject::sender() != nullptr);
_stream.setDevice(dynamic_cast<QTcpSocket*>(QObject::sender()));
//"startGame" is a button on the users Interface to start the game
connect(_ui->startGame, SIGNAL(returnPressed()), this, SLOT(gameStart()));
connect(QObject::sender(), SIGNAL(readyRead()), this, SLOT(receive()));
}


void disconnected() {
//"lePrint" could be any notificacion's message printes on the player's screen
   _ui->lePrint->appendPlainText("Disconnecting/Deleting Socket");
   _stream.device()->deleteLater();
}



void receive(){
_stream.setByteOrder(QDataStream::BigEndian);
quint8 cmd;
_stream>>cmd;

if(cmd==1){
if(_stream->device()->bytesAvailable()==3){

quint8 cmd, lenght, beginner, groupNumber;
_stream>>cmd, lenght, beginner, groupNumber;

// send the received data to logic, talk to Dmitry
// emit spielbeginn(cmd, lenght, beginner, groupNumber);
}
}


else if(cmd ==2){
if(_stream->device()->bytesAvailable()==2){

quint8 cmd= 2, lenght = 1, groupNumber;
_stream >>cmd>>lenght>>groupNumber;

// send the received data to logic, talk to Dmitry
// emit answerSpielbeginn(cmd, lenght, groupNumber);
}
}


else if(cmd ==3){
if(_stream->device()->bytesAvailable()==6){

quint8 cmd= 3, lenght = 5, column, row, endColumn, endRow, addInfo;
_stream >>cmd>>lenght>>column>>row>>endColumn>>endRow>>addInfo;

// send the received data to logic, talk to Dmitry
// emit receiveMove(cmd, lenght, column, row, endColumn, endRow, addInfo);
}
}


else if (cmd==4){
if(_stream->device()->bytesAvailable()==2){

quint8 cmd= 4,lenght = 1, status;
_stream >>cmd>>lenght>>status;

// send the received data to logic, talk to Dmitry
// emit responseMove(cmd, lenght, status);
}
}
}



void gameStart(quint8 beginner){
if(_isServer && cmd==1){

quint8 cmd = 1, lenght = 2, beginner, groupNumber = 3;
_stream <<cmd<<lenght<<beginner<<groupNumber;
}
connect(QObject::sender(), SIGNAL(readyRead()), this, SLOT(answerStart()));
}


void answerStart(){
if(_isClient && cmd==2){
quint8 cmd = 2, lenght = 1, groupNumber = 3;
_stream <<cmd<<lenght<<groupNumber;
}
connect(QObject::sender(), SIGNAL(readyRead()), this, SLOT(sendMove()));
}



void sendMove(quint8 cmd, quint8 lenght, quint8 column, quint8 row,
quint8 endColumn, quint8 endRow, quint8 addInfo){


if(cmd==3){
_stream <<cmd<<lenght<<column<<row<<endColumn<<endRow<<addInfo;
}
connect(QObject::sender(), SIGNAL(readyRead()), this, SLOT(sendAnswer()));
}



void sendAnswer(quint8 cmd, quint8 lenght, quint8 status){

if(cmd==4){
_stream <<cmd<<lenght<<status;
}
connect(QObject::sender(), SIGNAL(readyRead()), this, SLOT(sendMove()));
}
