#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QToolButton>
#include <QDialogButtonBox>
#include <QMouseEvent>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

struct figure
{
    int id;
    int id_old;
    QToolButton* btn;
    QIcon picture;
    int color;
};
class MainWindow : public QMainWindow
{
    Q_OBJECT
    QToolButton *field[64];
    figure fig[32];
    QIcon *icon;
    QToolButton *button;
    QDialogButtonBox *buttonBox;
    int x_0,y_0,width;
    int OK_x,OK_y,OK_w,OK_h;
    bool startChosen,endChosen;
    int move[2];
    int sel_fid;
    bool p1,p2;
    bool white,isWhiteTurn;



public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    void SetFigures();
    void MoveFigure(int fid);
    void UpdatePictures();
    void createFigures();
protected:
 //   void mousePressEvent(QMouseEvent *mouse) override;
    void resizeEvent(QResizeEvent *event) override;
public:



public slots:
    void log_on_p1(int n);
    void log_update(int bb[64]);
    void log_endgame();
    void onButtonClick();
    void on_buttonBox_accepted();
    void on_buttonBox_rejected();
public:
signals:
    void log_start(bool white);
    void log_p1(int ind_s, int prom);
    void log_p2(int ind_s,int ind_e, int prom);
    void resign(bool white);
private slots:
    void on_pushButton_clicked();
};
#endif // MAINWINDOW_H
