#include "game.h"
#include "schach.h"
#include "mainwindow.h"
#include <QWidget>
#include <QDebug>
#include <QApplication>
#include "config.h"

int main(int argc, char* argv[]){
    QApplication app(argc, argv);
    MainWindow mainwindow; //gui
    config_menu conf;
//    mainwindow.show();
    conf.show();
    Game game; //logic
    Chess chess; //network

    for(int i = 7; i >= 0; i--){ //debug
        QDebug d = qDebug();
        for (int j = 0; j < 8; j++){
            if(game.getTile(i * 8 + j)->isPiece())
                d << game.getTile(i * 8 + j)->getPiece()->getType() + game.getTile(i * 8 + j)->getPiece()->isWhite() * 0;
            else
                d << ".";
        }
    }
    qDebug() << "";
    //logic - network
    QObject::connect(&game, &Game::net_start, &chess, &Chess::gameStart);
    QObject::connect(&game, &Game::net_start_ok, &chess, &Chess::answerStart);
    QObject::connect(&game, &Game::net_move, &chess, &Chess::sendMove);
    QObject::connect(&game, &Game::net_move_ok, &chess, &Chess::sendAnswer);
    QObject::connect(&chess, &Chess::spielbeginn, &game, &Game::start_rec);
    QObject::connect(&chess, &Chess::answerSpielbeginn, &game, &Game::start_ok_rec);
    QObject::connect(&chess, &Chess::receiveMove, &game, &Game::move_rec);
    QObject::connect(&chess, &Chess::responseMove, &game, &Game::move_ok_rec);

    //logic - gui
    QObject::connect(&game, &Game::gui_on_p1, &mainwindow, &MainWindow::log_on_p1);
    QObject::connect(&game, &Game::gui_update, &mainwindow, &MainWindow::log_update);
    QObject::connect(&game, &Game::gui_endgame, &mainwindow, &MainWindow::log_endgame);
    QObject::connect(&mainwindow, &MainWindow::log_start, &game, &Game::start);
    QObject::connect(&mainwindow, &MainWindow::log_p1, &game, &Game::p1);
    QObject::connect(&mainwindow, &MainWindow::log_p2, &game, &Game::p2);
    QObject::connect(&mainwindow, &MainWindow::resign, &game, &Game::resign);


    //network - gui
    QObject::connect(&chess, &Chess::serverConnected, &conf, &config_menu::net_server_con);
    QObject::connect(&chess, &Chess::clientConnected, &conf, &config_menu::net_client_con);
    QObject::connect(&chess, &Chess::invalidPort, &conf, &config_menu::net_port_inv);
    //disconnect successful?
    QObject::connect(&conf, &config_menu::net_isServer, &chess, &Chess::setServerClient);
    QObject::connect(&conf, &config_menu::net_connect, &chess, &Chess::connectServerClient);
    QObject::connect(&conf, &config_menu::net_disconnect, &chess, &Chess::disconnectServerClient);
    QObject::connect(&conf, &config_menu::net_setPort, &chess, &Chess::getPort);
    QObject::connect(&conf, &config_menu::net_setIp, &chess, &Chess::getAddress);

    return app.exec();
}
