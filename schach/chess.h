#ifndef CHESS_H
#define CHESS_H

#include<QWidget>
#include<QNetwork/QTcpSocket>
#include<QDataStream>


namespace Ui{
class Chess;
}

class Chess : public QWidget{
Q_OBJECT

public:

explicit Chess(QWidget parent = *nullptr);
~Chess();

private slots:

void setServer();
void setClient();
void connectToServer();
void disconnectFromServer();
void enableConnection();
void disconnected();
void gameStart();
void receive();
void answerStart();
void sendMove();
void sendAnswer();


private:
Ui::Chess *_ui;
QDataStream _stream;
QTcpSocket _socket;
};
#endif // CHESS_H
