#ifndef GAME_H
#define GAME_H
#pragma once
#include <board.h>
#include <QObject>

struct Move {
public:
    Move(int p1, int p2, int p3, int p4, int lf, int prom);
    int p1; //pri piece
    int p2;
    int p3; //sec piece (taken + castling)
    int p4;
    int lf;
    int prom;
};

class Game: public QObject //main class to recieve / send signals to be connected in main.cpp
{
    Q_OBJECT

public:
    Game();
    ~Game();
    Piece* getPiece(int p);
    Tile* getTile(int p);

public:
signals:
    void net_start(quint8 beginner);
    void net_start_ok(quint8 b);
    void net_move(quint8 column, quint8 row,
                 quint8 endColumn, quint8 endRow, quint8 addInfo);
    void net_move_ok(quint8 status);
    void gui_on_p1(int n);
    void gui_update(int bb[64]);
    void gui_endgame();



public slots: //possible to just have them as public functions
    //much more here eg starting a game
    void start(bool w);
    void p1(int p, int prom); //pos 1 (select piece)
    void p2(int p1, int p2, int prom); //pos1 pos2 (move from - to)
    void start_rec(quint8 beginner, quint8 groupNumber);
    void start_ok_rec(quint8 groupNumber);
    void move_rec(quint8 column, quint8 row, quint8 endColumn, quint8 endRow, quint8 addInfo);
    void move_ok_rec(quint8 status);
    void resign(bool white);  //ending the game



signals: //response to slots
    void on_p1(int respose);
    void on_p2(int response);
    void on_resign();

private:
    bool white; //1 if we are white
    int turn;
    std::vector<Move> moves;
    Board* board;
    int Kingw;
    int Kingb;
    bool running;
    int lastFifty;
    //map posistion hash + amount for thrice same position
    int sel;

    void reset(); //we dont know white here
    int move(int p1, int p2, int prom);
    bool unmove();
    Tile* getKing(bool c); //for gui
    int isCheck(); //int for 3 types: no, white, black
    int isCheck(int p1, int p2);
    int isMate();
    int isMate(int p1, int p2);
    int isStalemate();
    int isStalemate(int p1, int p2);
    int isDead();
    int isDead(int p1, int p2);
    int isRepeat();
    int isRepeat(int p1, int p2);
    void mate(bool white);
    void draw();
};

#endif // GAME_H
