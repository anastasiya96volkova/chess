#ifndef SCHACH_H
#define SCHACH_H


#include <QMainWindow>
#include<QWidget>
#include<QTcpServer>
#include<QString>
#include<QTextStream>
#include<QNetworkInterface>
#include<QTcpSocket>
#include<QAbstractSocket>
#include<QDataStream>
#include<QDebug>


class Chess : public QWidget {
    Q_OBJECT

    QTcpSocket* _socket;

    QDataStream _stream;

    QTcpServer *server;

    bool _isServer;

    bool _connected = false;

    quint8 cmd;

    quint16 port;

    QString address;



public:

    Chess(QWidget *parent = nullptr);
    ~Chess();



public slots:

//____________________________SLOTS CALLEB BY UI__________________________________________

    //Ui calls this slot and tell me if the Player choosed the player or client mode
    void setServerClient(bool _isServer);


    //ui calls this slot when the Player pushes the Connect Button on the main window
    void connectServerClient();


    //Ui calls this slot if the player decides to disconnect from the game
    void disconnectServerClient();


    //Ui calls this slots to give me the port that the player wrote
    void getPort(quint16 port);


    //Ui calls this slot to give me the address in case that the Player chooses Client
    void getAddress(QString address);


//____________________________SLOTS CALLEB BY LOGIC__________________________________________


    //Logic sends a signal to start the game. The signal is gonna call my SLOT gameStart
    void gameStart(quint8 beginner);


    //Logic sends a signal to answer to start the game. The signal is gonna call my SLOT answerStart
    void answerStart();


    //Logic sends Netzwerk a signal to send a move. The signal is gonna call my SLOT sendMove
    void sendMove(quint8 column, quint8 row,
                  quint8 endColumn, quint8 endRow, quint8 addInfo);


    //Logic sends Netzwerk a signal to send an answer to send a move.
    //The signal is gonna call my SLOT sendAnswer
    void sendAnswer(quint8 status);


//____________________________SLOTS CALLEB BY NETZWERK__________________________________________

    //gets data from server or client
    void receive();

    //connects the player as the server
    void newConnection();

    //connects the player as the client
    void enableConnection();




signals:

//____________________________SIGNALS FOR LOGIC___________________________________________________

    // send the received data to logic for the cmd 1
    void spielbeginn(quint8 beginner, quint8 groupNumber);

    // send the received data to logic for the cmd 2
    void answerSpielbeginn(quint8 groupNumber);

    // send the received data to logic for the cmd 3
    void receiveMove(quint8 column, quint8 row, quint8 endColumn, quint8 endRow, quint8 addInfo);

    // send the received data to logic for the cmd 4
    void responseMove(quint8 status);




//____________________________SIGNALS FOR UI___________________________________________________

    //Player gave an invalid port, send signal to Ui
    void invalidPort();

    //Player succesfully connected as the server, send signal to Ui to print
    //out a message about the connection and to call a slot to start the game
    void serverConnected();

    //Player succesfully connected as the client, send signal to Ui to print
    //out a message about the connection
    void clientConnected();

    //send signal to Ui to print out a message about the disconnection
    //from Server
    void clientSuccesfullyDisconnected();

    //send signal to Ui to print out a message about the disconnection
    //of the server
    void serverSuccesfullyDisconnected();


};

#endif // SCHACH_H
